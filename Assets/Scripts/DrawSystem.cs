﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class DrawSystem : BaseDrawSystem
{
    public Color color;
    void OnTriggerEnter()
    {
        GameObject nGameObject = new GameObject();
        line = nGameObject.AddComponent<LineRenderer>();
        ConfigLineRenderer(line);
    }

    private void ConfigLineRenderer(LineRenderer nline)
    {
        Reset();
        nline.startWidth = 0.005f;
        nline.endWidth = 0.005f;
        nline.material.color = color;
    }

    void OnTriggerStay (Collider col) 
	{
		 Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 0.5f))
        {
            if (line.positionCount == 0 || Vector3.Distance(lastLinePos, hit.point) >= 0.005f)
                AddPointToLine(hit);                              
        }
	}


}
