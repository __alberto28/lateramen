﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseDrawSystem : MonoBehaviour
{
    #region Properties
    internal bool drawing { get { return line.positionCount > 0; } }
    internal Vector3 drawNormal { 
        get 
        {
            Vector3 l_newNormal = Vector3.zero;
            for (int i = 0; i < lineNormals.Count; i++)
            {
                l_newNormal += lineNormals[i];

            }
            return l_newNormal.normalized; 
        } 
    }
    internal Vector3 drawCenter { get { return line.GetPosition(0) + (line.GetPosition((int)lineNormals.Count / 2) - line.GetPosition(0)) / 2; } }
    

    internal Vector3 lastLinePos
    {
        get
        {
            if (line.positionCount > 0)
                return line.GetPosition(line.positionCount - 1);

            return Vector3.zero;
        }
    }

    internal LineRenderer line;
    internal List<Vector3> lineNormals = new List<Vector3>();

    #endregion    

    #region Methods

    void Awake()
    {
        line = GetComponent<LineRenderer>();
        if (line == null)
            line = gameObject.AddComponent<LineRenderer>();

        line.positionCount = 0;
    }

    protected void AddPointToLine(RaycastHit p_hit)
    {
        AddPointToLine(p_hit.point, p_hit.normal);
    }

    internal void AddPointToLine(Vector3 p_point, Vector3 p_normal)
    {
        line.positionCount++;
        Vector3 l_position = p_point + p_normal * 0.01F;
        lineNormals.Add(p_normal);
        line.SetPosition(line.positionCount - 1, l_position);
    }
    internal void Reset()
    {
       // Destroy(gameObject);
        line.positionCount = 0;
        lineNormals.Clear();
    }
    #endregion    

}
